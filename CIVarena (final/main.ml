open Graphics



let rec lit_tout f = 
(*Pour lire un fichier texte*)
  try
      let n = String.trim (input_line f) in
      n :: lit_tout f
  with End_of_file -> close_in f; [];;




let imagelib_vers_graphics im = 
 (*Pour lire une image rt l'utiliser *)
  let w, h = im.Image.width, im.Image.height in
  let m = Array.init h 
      (fun j ->
          Array.init w 
              (fun i -> Image.read_rgb im i j rgb)) in
  make_image m
;;





let attribuer_quatre_nb_diff_alea () =
(*Le but est d'attribuer 4 nombres aléatoirement différents 2 à 2 notés a, b, c et d et de les renvoyer en un tuple *)
  Random.self_init ();
  let a = Random.int 4 in

  let a' = ref (Random.int 4) in
  if a = !a' then while !a' = a do a' := Random.int 4 done;
  let b = !a' in

  let b' = ref (Random.int 4) in
  if !b' = b || !b' = a then while !b' = a || !b'=b do b' := Random.int 4 done;
  let c = !b' in

  let c' = ref (Random.int 4) in
  if !c'= a || !c' = b || !c' = c then while !c' = a || !c'=b || !c'=c do c' := Random.int 4 done;
  let d= !c' in
  (a,b,c,d)
;;

 



let perdu() =
(*Si la partie est perdue, on ouvre une fenêtre pour annoncer  la défaite et à la fermeture dea fenêtre, le jeu se termine
ne renvoie rien  *)
  open_graph " 1600x900"; (*X x Y*)
  set_color (rgb 0 0 0);
  let perdu = ImageLib_unix.openfile "lose.bmp" in
  let perdu_gx = imagelib_vers_graphics perdu in
  draw_image perdu_gx 0 0;
  set_window_title "PERDU";
  ignore (Graphics.read_key ())
;;


let gagne() =
(*L'équivalent de la fonction perdu mais pour une partie gagnée *)
  open_graph " 1600x900"; (*X x Y*)
  set_color (rgb 0 0 0);
  let gagne = ImageLib_unix.openfile "win.bmp" in
  let gagne_gx = imagelib_vers_graphics gagne in
  draw_image gagne_gx 0 0;
  set_window_title "GAGNE";
  ignore (Graphics.read_key ())
;;


let rencontre_DeFalcool pv_nous perso =
(*Le 3e prof rencontré, c'est la  fonction qui permet d'afficher les questions du prof ainsi que les réponses 
Prend en paramètres : pv_nous : int = donne le nombre de points de vi restants au joueur 
perso : int ref  = indique le personnage choisi par l joueur 
La fonction ne renvoie rien *)
  Random.self_init ();
  
  let tab_defa = lit_tout (open_in "questionsDef.txt") in (*strin list : [ligne1; ligne2; ... ; lignen], avec n le nombre de question qui est aussi le nombre de lignes *)
  let tab_question_defa = Array.make (List.length tab_defa) "0" in (*string Array avec n questions  *)
  let tab_reponse_defa = Array.make (List.length tab_defa) [|"0"|] in (*string Array Array, un tableau de tableau avec n tableaux qui comportent chacun 4 éléments = 4 reponses différentes *)

  let separation_ligne ligne ind = 
    let temp = String.split_on_char ';' ligne in (*separe la ligne ligne numero ind en prenant le premier element devant le ; qui sera la reponse et les autres elements entre ; seront les 4 reponses possibles*)

    Array.set tab_question_defa ind (List.hd temp);
    let temp2 = Array.make 4 "0" in
    for i = 0 to 3 do
      Array.set temp2 i (List.nth temp (i+1))
    done;
    Array.set tab_reponse_defa ind temp2;
  in

  for ind = 0 to ((List.length tab_defa)-1) do
    separation_ligne (List.nth tab_defa ind) ind
  done;
 (* Le but ce ces lignes est de prendre chaque éléments de la liste qui contient chaque ligne du fichier texte et d'en retirer la question puis les 4 réponses qui suivent et de les mettre respectivement dans un tableau de questions et un de réponses sachant que le fichier texte est de la forme :
 question1;reponse1;reponse2;reponse3;reponse4;\n
question2;reponse1...*)



  let rec defalco ques_deja_vu pv_prof pv_nous =
  (*Le but de cette fonction auxiliaire est d'afficher à chaque fois la question ainsi que les réponses qui vont avec
elle prend en paramètres : 
ques_deja_vu : string Array = un tableau de "0" et/ou de "1" qui fait la meme taille que le nombre de questions, si la question d'indice 3 a déjà été posée alors ques_deja_vu.(3)="1", "0" sinon, cela permet de ne pas poser plusieurs fois la même question
le tableau est donc initialement remplie de "0"
pv_prof : int = indique e nombre de points de vie du prof, commence ici à 12, peut décroître ou ne pas changer de valeur
pv_nous : int = même  chose que pv_prof mais indique les points de vie du joueur
La fonction s'appelle elle même mais ne renvoie jusqu'à rien, appelle gagne () ou perdu () qui mettront fin au je
*)
  
    open_graph " 1600x900"; (*X x Y*)
    set_color (rgb 0 0 0);


    let bmp = ImageLib_unix.openfile "defalcool.bmp" in
    let bmp_gx = imagelib_vers_graphics bmp in
    draw_image bmp_gx 0 0;
(*On affiche l'image de fond *)
    

(*On affiche ici l'image correspondant au personnage  choisi par le joueur choisi au début du jeu *)
    if !perso = 1 then begin 
      let bmp2 = ImageLib_unix.openfile "perso1.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end

    else if !perso = 2 then begin 
      let bmp2 = ImageLib_unix.openfile "perso2.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end
    
    else begin 
      let bmp2 = ImageLib_unix.openfile "perso3.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end;


    set_window_title "DEFALCOOL";

(*On teste si le jeu est perdu ou non, que se soit une victoire ou une défaite l'appel aux fonction gagne et perdu mettra fin au jeu *)
    if pv_prof <=0 then gagne()

    else if pv_nous <= 0 then perdu()

    else begin
(*Si il reste des pv aux 2  adversaires alors le jeu continue, on pose des questions *)

      let indice = ref (Random.int (Array.length tab_question_defa)) in

      if ques_deja_vu.(!indice) = "0" then ques_deja_vu.(!indice) <- "1"
      else 
        while ques_deja_vu.(!indice) = "1" do
          indice := Random.int (Array.length tab_question_defa)
        done;
      
      
      ques_deja_vu.(!indice) <- "1";
(*On teste si la question a deja été posée et on change la question et la valeur dans ques_deja_vu à l'indice correspondant  le cas échéant *)

      moveto(300)(100);
      draw_string tab_question_defa.(!indice); 
(*On affiche la question *)
      let un, deux, trois, quatre = attribuer_quatre_nb_diff_alea () in
      moveto(300)(637);
      draw_string tab_reponse_defa.(!indice).(un);
      moveto(300)(512);
      draw_string tab_reponse_defa.(!indice).(deux);
      moveto(300)(387);
      draw_string tab_reponse_defa.(!indice).(trois);
      moveto(300)(262);
      draw_string tab_reponse_defa.(!indice).(quatre);

(*et on affiche les réponses 
la variable un : int  correspond a la reponse  la  plus en haut, elle peut avoir pour valeur 0,1,2 ou 3 qui correspond à l'indice de la reponse dans le tableau contenant les reponses 
c'est la meme chose pour les variables deux, trois et quatres, il ne peut y avoir de doublon et l'ordre est donné aléatoirement   *)


(*La boucle qui suit va permettre d'attendre que le joueur clique sur une réponse et changer la valeur de reponse_joueur en fonction de là où a cliqué le joueur  *)
      let reponse_joueur = ref 5 in
      while !reponse_joueur = 5 do
        let s = wait_next_event [Button_down] in
        if s.button = true
          then begin
              let x,y=mouse_pos () in
              if x <= 400 && x >= 100 then begin
                if y >= 200 && y<= 325 then reponse_joueur := quatre
                else if y >= 325 && y<= 450 then reponse_joueur := trois
                else if y >= 450 && y<= 575 then reponse_joueur := deux
                else if y >= 575 && y<= 700 then reponse_joueur := un
                end;
          end;

      done;
      
      (*La bonne reponse est toujours celle d'indice 0 dans le tableau de reponses, o teste ainsi si la reponse donnée par le joueur est bonne ou non et on affiche le nombre de pv restant du joueur et du professeur, on attend que le joueur appuie sur une touche pour relancer la fonction auxiliaire avec les pv du prof ou du joueur décrémenté selon le cas et le tableau de questions deja posées  *)
      match !reponse_joueur with
      | 0 -> moveto(400)(300); set_color (rgb 0 255 0);  draw_string "BONNE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int pv_nous) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int (pv_prof -1)) ;
            ignore (Graphics.read_key ()); defalco ques_deja_vu (pv_prof-1) pv_nous

      | _ -> moveto(400)(300); set_color (rgb 255 0 0);  draw_string "MAUVAISE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int (pv_nous-1)) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int pv_prof) ;
            ignore (Graphics.read_key ()); defalco ques_deja_vu pv_prof (pv_nous -1)
    end;

(*On sort de la fonction est on l'appelle une première fois avec un tableau de question déjà vu rempli de "0" signifiant qu'aucune question n'a  encore été posée *)
  in defalco (Array.make (Array.length tab_question_defa) "0") 12 pv_nous

(*Ce professeur étant le 3e il a plus de PV que ls prédécesseurs, il commence avec 12 pv *)
;;








let rencontre_Bouchetiflore pv_nous perso =

(*Fonctionne de 'a me maniere que rencontre_Defalcool et prend les memes parametres
ne renvoie rien mais fait appel a la fonction perdu si les  pv du joueur sont egaux à 0, si les pv du profs sont nuls alors appelle la fonctions rencontre_Deflacool en lui passant en paramètres  les pv restant au joueur et perso *)
  Random.self_init ();
  
  let tab_bou = lit_tout (open_in "questionsBou.txt") in (*liste : [ligne1; ligne2; ... ; lignen]*)
  let tab_question_bou = Array.make (List.length tab_bou) "0" in (*string Array*)
  let tab_reponse_bou = Array.make (List.length tab_bou) [|"0"|] in (*string Array Array*)

  let separation_ligne ligne ind = 
    let temp = String.split_on_char ';' ligne in (*separe la ligne ligne numero ind en prenant le premier element devant le ; qui sera la reponse et les
       autres elements entre ; seront les 4 reponses possibles*)
    Array.set tab_question_bou ind (List.hd temp);
    let temp2 = Array.make 4 "0" in
    for i = 0 to 3 do
      Array.set temp2 i (List.nth temp (i+1))
    done;
    Array.set tab_reponse_bou ind temp2;
  in

  for ind = 0 to ((List.length tab_bou)-1) do
    separation_ligne (List.nth tab_bou ind) ind
  done;
 (*met dans les tableau toutes les lignes*)


  let rec bouchet ques_deja_vu pv_prof pv_nous =
  
    open_graph " 1600x900"; (*X x Y*)
    set_color (rgb 0 0 0);

  
    let bmp = ImageLib_unix.openfile "bouchetiflor.bmp" in
    let bmp_gx = imagelib_vers_graphics bmp in
    draw_image bmp_gx 0 0;

    
    if !perso = 1 then begin 
      let bmp2 = ImageLib_unix.openfile "perso1.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end

    else if !perso = 2 then begin 
      let bmp2 = ImageLib_unix.openfile "perso2.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end
    
    else begin 
      let bmp2 = ImageLib_unix.openfile "perso3.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end;


    set_window_title "BOUCHETIFLOR";
    if pv_prof <=0 then begin 
      moveto(400)(300) ; 
      set_color (rgb 0 255 0); 
      draw_string "VICTOIRE !! Passons aux choses serieuses, le prochain prof ! Appuiez sur entree pour continuer" ;
      ignore (Graphics.read_key ()) ; 
      rencontre_DeFalcool pv_nous perso end

    else if pv_nous <= 0 then perdu()

    else begin


      let indice = ref (Random.int (Array.length tab_question_bou)) in

      if ques_deja_vu.(!indice) = "0" then ques_deja_vu.(!indice) <- "1"
      else 
        while ques_deja_vu.(!indice) = "1" do
          indice := Random.int (Array.length tab_question_bou)
        done;
      
      
      ques_deja_vu.(!indice) <- "1";

      moveto(300)(100);
      draw_string tab_question_bou.(!indice);
      let un, deux, trois, quatre = attribuer_quatre_nb_diff_alea () in
      moveto(300)(637);
      draw_string tab_reponse_bou.(!indice).(un);
      moveto(300)(512);
      draw_string tab_reponse_bou.(!indice).(deux);
      moveto(300)(387);
      draw_string tab_reponse_bou.(!indice).(trois);
      moveto(300)(262);
      draw_string tab_reponse_bou.(!indice).(quatre);

      (*affiche les quatres question et la reponse*)

      let reponse_joueur = ref 5 in
      while !reponse_joueur = 5 do
        let s = wait_next_event [Button_down] in
        if s.button = true
          then begin
              let x,y=mouse_pos () in
              if x <= 400 && x >= 100 then begin
                if y >= 200 && y<= 325 then reponse_joueur := quatre
                else if y >= 325 && y<= 450 then reponse_joueur := trois
                else if y >= 450 && y<= 575 then reponse_joueur := deux
                else if y >= 575 && y<= 700 then reponse_joueur := un
                end;
          end;

      done;
      
      match !reponse_joueur with
      | 0 -> moveto(400)(300); set_color (rgb 0 255 0);  draw_string "BONNE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int pv_nous) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int (pv_prof -1)) ;
            ignore (Graphics.read_key ()); bouchet ques_deja_vu (pv_prof-1) pv_nous
      | _ -> moveto(400)(300); set_color (rgb 255 0 0);  draw_string "MAUVAISE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int (pv_nous-1)) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int pv_prof) ;
            ignore (Graphics.read_key ()); bouchet ques_deja_vu pv_prof (pv_nous -1)
    end;
  in bouchet (Array.make (Array.length tab_question_bou) "0") 6 pv_nous
;;









let rencontre_Aufrancsucces perso =
(* C'est le 1e professeur rencontré par le joueu Fonctionne de la même manière que rencontre_Bouchetiflor mais ne prend en paramètre que le personnage choisi par le joueur car les pv du joueur sont au maximum c'est a dire 10 
ne renvoie rien
fait appel a perdu si les pv du joueur tombent a 0, ou a rencontre_Bouchetiflor si les pv du prof tombent a 0 *)
  Random.self_init ();
  
  let tab_auf = lit_tout (open_in "questionsAuf.txt") in (*liste : [ligne1; ligne2; ... ; lignen]*)
  let tab_question_auf = Array.make (List.length tab_auf) "0" in (*string Array*)
  let tab_reponse_auf = Array.make (List.length tab_auf) [|"0"|] in (*string Array Array*)

  let separation_ligne ligne ind = 
    let temp = String.split_on_char ';' ligne in (*separe la ligne ligne numero ind en prenant le premier element devant le ; qui sera la reponse et les
       autres elements entre ; seront les 4 reponses possibles*)
    Array.set tab_question_auf ind (List.hd temp);
    let temp2 = Array.make 4 "0" in
    for i = 0 to 3 do
      Array.set temp2 i (List.nth temp (i+1))
    done;
    Array.set tab_reponse_auf ind temp2;
  in

  for ind = 0 to ((List.length tab_auf)-1) do
    separation_ligne (List.nth tab_auf ind) ind
  done;
 (*met dans les tableau toutes les lignes*)


  let rec aufranc ques_deja_vu pv_prof pv_nous =
  
    open_graph " 1600x900"; (*X x Y*)
    set_color (rgb 0 0 0);


    let bmp = ImageLib_unix.openfile "aufrancsucces.bmp" in
    let bmp_gx = imagelib_vers_graphics bmp in
    draw_image bmp_gx 0 0;

    
    if !perso = 1 then begin 
      let bmp2 = ImageLib_unix.openfile "perso1.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end

    else if !perso = 2 then begin 
      let bmp2 = ImageLib_unix.openfile "perso2.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end
    
    else begin 
      let bmp2 = ImageLib_unix.openfile "perso3.bmp" in
      let bmp2_gx = imagelib_vers_graphics bmp2 in
      draw_image bmp2_gx 0 200 end;


    set_window_title "AUFRANCSUCCES";
    if pv_prof <=0 then begin 
      moveto(400)(300) ; 
      set_color (rgb 0 255 0); 
      draw_string "VICTOIRE !! Passons aux choses serieuses, le prochain prof ! Appuiez sur entree pour continuer" ;
      ignore (Graphics.read_key ()) ; 
      rencontre_Bouchetiflore pv_nous perso end

    else if pv_nous <= 0 then perdu()

    else begin


      let indice = ref (Random.int (Array.length tab_question_auf)) in

      if ques_deja_vu.(!indice) = "0" then ques_deja_vu.(!indice) <- "1"
      else 
        while ques_deja_vu.(!indice) = "1" do
          indice := Random.int (Array.length tab_question_auf)
        done;
      
      
      ques_deja_vu.(!indice) <- "1";

      moveto(300)(100);
      draw_string tab_question_auf.(!indice);
      let un, deux, trois, quatre = attribuer_quatre_nb_diff_alea () in
      moveto(300)(637);
      draw_string tab_reponse_auf.(!indice).(un);
      moveto(300)(512);
      draw_string tab_reponse_auf.(!indice).(deux);
      moveto(300)(387);
      draw_string tab_reponse_auf.(!indice).(trois);
      moveto(300)(262);
      draw_string tab_reponse_auf.(!indice).(quatre);

      (*affiche les quatres question et la reponse*)

      let reponse_joueur = ref 5 in
      while !reponse_joueur = 5 do
        let s = wait_next_event [Button_down] in
        if s.button = true
          then begin
              let x,y=mouse_pos () in
              if x <= 400 && x >= 100 then begin
                if y >= 200 && y<= 325 then reponse_joueur := quatre
                else if y >= 325 && y<= 450 then reponse_joueur := trois
                else if y >= 450 && y<= 575 then reponse_joueur := deux
                else if y >= 575 && y<= 700 then reponse_joueur := un
                end;
          end;

      done;
      
      match !reponse_joueur with
      | 0 -> moveto(400)(300); set_color (rgb 0 255 0);  draw_string "BONNE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int pv_nous) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int (pv_prof -1)) ;
            ignore (Graphics.read_key ()); aufranc ques_deja_vu (pv_prof-1) pv_nous
      | _ -> moveto(400)(300); set_color (rgb 255 0 0);  draw_string "MAUVAISE REPONSE / " ; draw_string"PV restant "; draw_string (string_of_int (pv_nous-1)) ; draw_string" / PV restant de l'ennemi "; draw_string (string_of_int pv_prof) ;
            ignore (Graphics.read_key ()); aufranc ques_deja_vu pv_prof (pv_nous -1)
    end;
  in aufranc (Array.make (Array.length tab_question_auf) "0") 3 10
;;












let rec debut () =
(*C'est la 2e fonction a être appelée, elle permet au joueur de choisir le personnage qui le représentera 
ne prend aucun paramètre 
renvoie perso : int ref  qui correspondra au personnage choisi par le joueur *)
  open_graph " 1600x900"; (*X x Y*)
  let bmp = ImageLib_unix.openfile "test_pour_jeu.bmp" in
  let bmp_gx = imagelib_vers_graphics bmp in
  draw_image bmp_gx 0 0;
  set_window_title "Choisir son personnage";

  let perso = ref 0 in (*pour savoir quel perso on choisi*)

  while !perso = 0 do
    let s = wait_next_event [Button_down] in
      if s.button = true
      then begin
          let x,y=mouse_pos () in
          if x <= 450 && x >= 250 && y >= 150 && y<= 650 then perso := 1
          else if x <= 900 && x >= 700 && y >= 150 && y<= 650 then perso := 2
          else if x <= 1350 && x >= 1150 && y >= 150 && y<= 650 then perso := 3
      end;

  done;
(*Change la valeur de perso en fonction de la ou a clique le joueur *)



  if !perso = 1 then draw_rect 200 100 300 600
  
  else if !perso = 2 then draw_rect 650 100 300 600
  
  else draw_rect 1100 100 300 600;


(*Demande au joueur s'il valide son choix de personnage, si non, recommence en appelant la fonction debut () *)
  set_color white ;
  fill_rect 600 300 400 300; 
  set_color black ;
  draw_rect 750 350 60 30;
  draw_rect 790 350 60 30;
  moveto(770)(500);
  draw_string "Etes-vous sur ?";

  moveto(765)(360);
  draw_string "OUI";
  moveto(815)(360);
  draw_string "NON";
  let clique = ref 0 in

  moveto(1350)(870);
  draw_string "Appuyez sur entree pour confirmer";

  while !clique = 0 do
    let s = wait_next_event [Button_down] in
      if s.button = true
      then begin
          let x,y=mouse_pos () in
          if x <= 810 && x >= 750 && y >= 350 && y<= 380 then clique := 1 
          else if x <= 850 && x >= 790 && y >= 350 && y<= 380 then clique := 2
      end;
    done;
    ignore (Graphics.read_key ());
  if !clique = 2 then debut ()
  else perso

(*renvoie perso : int ref si le joueur est sûr de lui *)
;;




let presentation () =
(*Ne prend  aucun paramètre , ne renvoie rien, arfiche des images pour contextualiser le jeu et expliquer brièvement les règles *)
  open_graph " 1600x900"; (*X x Y*)
  let continue = ref true in
  let bmp = ImageLib_unix.openfile "presentation1.bmp" in
  let bmp_gx = imagelib_vers_graphics bmp in
  draw_image bmp_gx 0 0;
  set_window_title "Présentation";
  while !continue do
  let s = wait_next_event [Button_down] in
    if s.button = true then begin
    clear_graph () ;
    let bmp = ImageLib_unix.openfile "presentation2.bmp" in
    let bmp_gx = imagelib_vers_graphics bmp in
    draw_image bmp_gx 0 0;
    continue := false;
    end;
  done;

  continue := true;
  while !continue do
    let s = wait_next_event [Button_down] in
    if s.button = true then begin
      clear_graph () ;
      let bmp = ImageLib_unix.openfile "presentation3.bmp" in
      let bmp_gx = imagelib_vers_graphics bmp in
      draw_image bmp_gx 0 0;
      continue := false;
      end;
    done;

  continue := true;
  while !continue do
    let s = wait_next_event [Button_down] in
    if s.button = true then begin
      clear_graph () ;
      let bmp = ImageLib_unix.openfile "presentation4.bmp" in
      let bmp_gx = imagelib_vers_graphics bmp in
      draw_image bmp_gx 0 0;
      continue := false;
      end;
    done;

  continue := true;
  while !continue do
    let s = wait_next_event [Button_down] in
    if s.button = true then begin
      clear_graph () ;
      let bmp = ImageLib_unix.openfile "presentation5.bmp" in
      let bmp_gx = imagelib_vers_graphics bmp in
      draw_image bmp_gx 0 0;
      continue := false;
      end;
    done;

  continue := true;
  while !continue do
    let s = wait_next_event [Button_down] in
    if s.button = true then begin
      clear_graph () ;
      let bmp = ImageLib_unix.openfile "presentation6.bmp" in
      let bmp_gx = imagelib_vers_graphics bmp in
      draw_image bmp_gx 0 0;
      continue := false;
      end;
    done;
    ignore (Graphics.read_key ());
;;


let () =
(*C'est la variable permettant l'exécution du jeu *)
  presentation ();
  let perso = debut() in
  rencontre_Aufrancsucces perso

;;

