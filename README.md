# CIV arena
    


## Rules
Cher.e élève, vous vous retrouvez aujourd'hui face à une étape importante dans la vie d'un élève en MP2I au CIV : l'anère des MP2I du CIV autrement appelée CIVarena (car oui ici on est bilingue).
Votre objectif ? Défier chaque professeur dans des duels en 1V1 durant lesquels vous allez devoir vous jeter corps et âme afin de passer au niveau suivant. Attention, votre cerveau va carburer car ces duels
s'organisent sous forme de qcm posé par les professeur à l'élève que vous êtes.
Une bonne réponse, et c'est 1 PV en moins pour votre adversaire. Inversement, une mauvaise réponse vous 
enlèvera 1 PV.
Si vous n'avez plus de vie, vous perdez :/
Mais si le professeur n'a plus de vie, vous accédez au droit de passer au prochain jusqu'à finir le jeu !
Choisissez votre personnage au lancement du jeu et venez défier les professeurs des MP2I !
Bonne chance et bon courage :)
Indications : à chaque clic de souris sur une réponse ou un choix, veuillez également appuyer sur une touche de votre clavier. Merci !


## Progress of the project
- Creation of the Framagit project
- Ecriture du cahier des charges
- j'ai fait un debut de truc qui ressemble a la fonction pour choisir son perso au depart (hélène)
- "plan" du code avec certaines fonctions, type et tableau (sasha)
- code pour lire et trier dans des listes un fichiers txt avec Q&A (lucie)
- finition sur la lecture et la gestion du txt vers des array (lucie)
- gestion des questions/réponses et avancement sur la structure du jeu (hélène)
- prototype pour le deplacement (lucie)
- gros avancement général (sasha et hélène)
- abandon du déplacement :')
- gros avancement sur visuel et interface du jeu (lucie)
- création d'un dossier avec les élements les plus avancés et les fichiers nécessaires au jeu
- finalistaion interface de jeu (sasha)
- remplissage des fichiers de questions/réponses (lucie, sasha et hélène)
- dernière finition visuelle (sasha et hélène)

## Used Library
- Graphics
- Random

## Descriptif
L'idée originale était de faire une "arène" ou le joueur ferait des faces à faces contre des "ennemis" (les profs des MP2I) à base de QCM. On a pensé ajouter des déplcements masi l'idée a rapidement été abandonnée car nous avions d'autres problèmes à gérer avec le jeu initial. Nous avions ausi pensé à étoffer le jeu avec des bonus et des évènements spéciaux mais cela a aussi été abandonné par manque de temps.
Donc le jeu est au final une suite de "combat" en tour par tour avec des questions aléatoires et des points de vie pour le joueur et l'ennemi.

Les fonctions pour chacun des professeurs sont quasiment identiques mais certaines différences notables nous ont poussé à tout de même laisser les trois fonctions.
De plus, ces fonctions sont subdivisées en fonctions axiliaires pour organiser et rendre plus lisible le tout.
Certaines parties du code reste peu lisible par manque de choix.

Durant le projet, nous ne nous sommes pas vraiment repartie les tâche, nous reprenions le code précedemment uploader et nous l'avancions ainsi.

Nous avons donc tout réalisé à l'aide de graphics, choix qui a été pris très rapidement au début du projet.


## Problèmes rencontrés
- L'affichage d'image nous a posé quelques problèmes au départ
- Nous n'avons pas réussi à trouver un autre moyen optimal d'actualiser le jeu sans faire fermer la fenêtre par le joueur en appuyant sur une touche ni en superposant des images sur la fenêtre déjà ouverte
